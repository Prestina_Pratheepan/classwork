public class MethodWithParameters1{
	
		
		public void subNum(int x , int y){

		int result = x - y; 
		
		
		System.out.println("result: " + result);
	}
	
	public static void main(String[] args){
		
		int num1 = 30;
		int num2 = 20;
		
		MethodWithParameters demo = new MethodWithParameters();
		
		System.out.println(demo); 
		
		demo.subNum(num1 , num2); 
	}
}