public class MethodWithParameters{
	
		
		public void addNum(int x , int y){

		int result = x + y; 
		
		
		System.out.println("result: " + result);
	}
	
	public static void main(String[] args){
		
		int num1 = 20;
		int num2 = 30;
		
		MethodWithParameters demo = new MethodWithParameters();
		
		System.out.println(demo); 
		
		demo.addNum(num1 , num2); 
	}
}