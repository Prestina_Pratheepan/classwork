public class MethodDemo1{
	
		int x = 10;
		int y = 20;
		
	public void addNum(){

		int result = x + y; //call variable into non-static method
		
		
		System.out.println("result: " + result);
	}
	
	public static void main(String[] args){
		
		MethodDemo1 demo = new MethodDemo1(); //create an object
		
		System.out.println(demo.x); //call variable into static method
		demo.addNum(); //invoke a local method
	}
}