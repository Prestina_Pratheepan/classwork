public class WhileLoopEven{
	public static void main(String args[]){
		
		int num = 10;
		int i = 2;
		int sum = 0;
		
		
		while (i <= num){
			
			sum = sum + i;
			// sum+= i;
			
			// i=i+2;
			i += 2;
		}
			
			System.out.println("Sum of first " + num + " natural even number is : " + sum);
			
			
	}
}