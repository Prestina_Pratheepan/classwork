public class JavaMethodDemo{
	
	//simple java method
	public void sayHello(){
		System.out.print("Hello BCAS");
	}
	
	public void printWelcome(){
		System.out.println("Welcome");
	}
	
	public static void main(String[] args){
		JavaMethodDemo demo = new JavaMethodDemo(); //creating object
		
		demo.sayHello(); //calling method by object
		demo.printWelcome();
	}
}